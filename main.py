#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This is the main script that decides what type of notification
and when is executed.
"""
# Imports
from schedule import run_pending, every
from time import sleep

# Personal Imports
from conf.conf import (
    conf_dir,
    conf_file,
    key_file,
    crypt_file,
    tmp_crypt,
)
from helpers.io import IO
from helpers.file import FileInfo, Location
from helpers.wizard_home import WizardHome
from send_ntfy import send_file
from send_email import send_mail


loc = Location()
io = IO()
fi = FileInfo()
wh = WizardHome()


__author__ = "Troy Franks"
__version__ = "2023-05-15"

# Global Variables
home = loc.home_dir()
settings = io.open_settings(conf_dir, conf_file)


def runtime():
    return settings["runtime"]


def main_fun(
    send_email: bool,
    send_ntfy: bool,
):
    if send_email and send_ntfy:
        send_file(
            settings,
            home,
        )
        send_mail(settings)

        print("send email and send ntfy")
    elif send_ntfy:
        print("send ntfy")
        send_file(
            settings,
            home,
        )
    elif send_email:
        send_mail(settings)
        print("send email")
    else:
        print("Either email or ntfy have to be enabled")


dir_exist = fi.check_dir(conf_dir)
if dir_exist is False:
    wh.config_setup_backup_notify(
        conf_dir=conf_dir,
        conf_file=conf_file,
        key_file=key_file,
        crypt_file=crypt_file,
        tmp_crypt=tmp_crypt,
        email_status=False,
    )
every().day.at(str(runtime())).do(
    lambda: main_fun(
        send_email=settings["send_email"],
        send_ntfy=settings["send_ntfy"],
    )
)

if __name__ == "__main__":
    try:
        print("waiting on timer")
        while True:
            run_pending()
            sleep(1)
    except KeyboardInterrupt as e:
        print(e)
        print("Interrupted")
