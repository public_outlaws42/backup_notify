# backup-notify

I use this to send a notification daily of the last 50 lines of the backup logs from our computers to verify that nothing has gone wrong.  This program will also check the age of the log files give error if the logs are to old indicating that new logs are not being created.  This will tell that there is some issue in the backup process.  

## Prerequisites

- `Python 3.8` at a minimum - In Linux this should not be a problem most recent distros come with at least python 3.8. 
- `pip` - pip is the package manager for python.  


## conf.py
You will need to create a directory in root of cloned project called `conf` and create a file called `conf.py`.  So it will be like this `conf/conf.py`
This file will help setup the settings files that will store your configuration. You will need 5 variables in this file. The variables need to be as listed below but there values can be anything. They just need to have the extenstion as shown below.


```python

conf_dir: str = ".config/backup-notify"
conf_file: str = "settings.yaml"
key_file: str = ".any_name.key"
crypt_file: str = ".any_name.yaml"
tmp_crypt: str = ".any.yaml"

```


## Installing 

Installing python 3, pip on the raspberry pi or any Debian based Linux computer
```bash
sudo apt-get install python3 python3-pip 

```
Run this from a terminal in the directory where you want to clone the repository to your local computer

```bash
git clone https://gitlab.com/public_outlaws42/backup_notify.git


```
Change into the project main directory

```bash
cd backup_notify

```

This project utilizes a submodule for the helpers library. to pull these files in you will need to run 2 commands.

first we need it intiate the submodule
```bash
git submodule init
 
```
next you need to pull the files from the helpers repository
```bash
git submodule update
 
```

Then run this command to install the modules needed

```bash
pip3 install -r requirements.txt

```

You will have to make the python files executable.

```bash
chmod u+x *.py

```

## To run

**main.py**  This is the main file and is what you would run. 

The best way to run this is to make this a system service that starts at boot time. 

To run the script temporarily do the following. You want to run manually at least once to setup the config file. 
```bash
./main.py

```

If the script doesn't see a `conf_dir` or `conf_file` it will automatically run a commandline wizard to help setup a config file. Once the config file is created the wizard won't run anymore.

**Note:** You should be able to run this code on Windows and MacOS as well as long as you have the 
prerequisites installed. Running the python files and starting everything at boot time will be different for those platforms but it should be possible.

## License
GPL 2.0