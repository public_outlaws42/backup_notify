#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This script sends notifications with a section of backup log files through the
ntfy service.
"""
# Imports
import requests

# Personal Imports
from create_log import create_file

__author__ = "Troy Franks"
__version__ = "2023-05-15"

# Global Variables


def send_file(settings, home):
    config = settings
    create_file()
    logs = config["logs"]
    topic = config["topic"]
    desc = config["description"]

    for i in range(len(logs)):
        requests.put(
            f"https://ntfy.sh/{topic}",
            data=open(f"{home}/{logs[i]}.txt", "rb"),
            headers={
                "Filename": f"{logs[i]}.txt",
                "Description": f"{desc[i]}",
            },
        )


if __name__ == "__main__":
    print("Made to run as a module")
