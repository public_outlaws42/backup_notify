#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This creates a copy of a log file with only the last so many lines
for sending with notifications.
"""

# Imports
from getpass import getuser

# Personal Imports
from conf.conf import conf_dir, conf_file
from helpers.file import FileInfo
from helpers.io import IO

io = IO()
fi = FileInfo()

__author__ = "Troy Franks"
__version__ = "2024-07-28"

# Global Variables
username = getuser()
settings = io.open_settings(conf_dir, conf_file)


def create_file():
    """
    Creates all the log files that are listed in the settings file
    """
    logs = settings["logs"]
    lines = settings["lines"]
    if "description" in settings:
        desc = settings["description"]
    else:
        desc = []
        for i in range(len(logs)):
            desc.append("NA")

    for i in range(len(logs)):
        body = file_body(filename=logs[i], lines=lines, desc=desc[i])
        io.save_file(
            fname=f"{logs[i]}.txt",
            content=body,
            fdest="home",
            mode="w",
        )


def file_body(
    desc: str,
    filename: str,
    lines: int,
):
    """
    Creates the log file body. with
    File age: how old the log file is with username.
    Filename: name of log file.
    Description: Description of what the log file is for.
    The truncated log file.
    """
    age: int = fi.check_file_age(filename, "home")
    if age >= 24:
        body: str = (
            f"The log file {filename} for "
            f"{username} is {age} hours old check backup"
        )
    else:
        truncated_file = io.last_n_lines(
            fname=filename,
            lines=lines,
            fdest="home",
        )
        body = f"File age: {age} hours for user {username}\nFilename: {filename}\nDescription: {desc}\n\n{truncated_file}"
    return body


if __name__ == "__main__":
    print("create_log")
