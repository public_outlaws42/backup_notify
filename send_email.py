#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This script creates and sends notifications with a section of backup log file
embeded in the email.
"""

# Imports
from getpass import getuser

# Personal Imports
from conf.conf import (
    conf_dir,
    key_file,
    crypt_file,
)
from create_log import file_body
from helpers.io import IO
from helpers.file import FileInfo
from helpers.encrypt import Encryption
from helpers.email import Mail

ml = Mail()
en = Encryption()
io = IO()
fi = FileInfo()

__author__ = "Troy Franks"
__version__ = "2023-05-15"

# Global Variables
username = getuser()


def send_mail(config_setting):
    kf = f"{conf_dir}/{key_file}"
    ef = f"{conf_dir}/{crypt_file}"
    config = config_setting
    st = config["sendto"]
    logs = config["logs"]
    lines = config["lines"]

    for i in range(len(logs)):
        body = file_body(
            filename=logs[i],
            lines=lines,
        )
        sub = f"Backup Log: for {username} (Log file: {logs[i]})"
        key = io.open_file(
            fname=kf,
            fdest="home",
            mode="rb",
        )
        login = en.decrypt_login(
            key=key,
            e_fname=ef,
            fdest="home",
        )

        ml.mail(
            body=body,
            subject=sub,
            send_to=st,
            login=login,
        )


if __name__ == "__main__":
    print("send_email")
